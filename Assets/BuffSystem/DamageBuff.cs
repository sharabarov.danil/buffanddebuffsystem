﻿
using UnityEngine;
namespace BuffSystem
{
    public class DamageBuff : IBuff
    {

        private readonly int _damageBonus;
        private string _id;

        public DamageBuff(int damageBonus)
        {
            _damageBonus = damageBonus;
            _id = this.ToString().GenerateUUID();
        }

        public string ID => _id;

        public CharacterStats ApplyBuff(CharacterStats baseStats)
        {
            var newStats = baseStats;
            newStats.Damage = Mathf.Max(newStats.Damage + _damageBonus, 0);

            return newStats;
        }
    }
}

