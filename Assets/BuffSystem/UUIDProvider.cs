﻿using System;
using System.Security.Cryptography;
using System.Text;
namespace BuffSystem
{
    public static class UUIDProvider
    {
        public static string GenerateUUID(this string subsetName)
        {
            var random = new byte[16];
            var rng = new RNGCryptoServiceProvider();
            rng.GetBytes(random);

            var nameByte = Encoding.ASCII.GetBytes(subsetName);

            var mutableLength = nameByte.Length > 4 ? 4 : nameByte.Length;
            for (var i = 0; i < mutableLength; i++)
            {
                random[i] = nameByte[i];
            }

            var bytes = BitConverter.ToString(random);
            var split = bytes.Split('-');

            var result = string.Empty;
            for (var i = 0; i < split.Length; i++)
            {
                result += split[i];
                if (i is 3 or 5 or 7 or 9) result += "-";
            }

            result = result.ToLower();
            return result;
        }
    }
}