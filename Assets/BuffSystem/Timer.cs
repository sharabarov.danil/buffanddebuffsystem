﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UnityEngine;
namespace BuffSystem.Example
{
    public class Timer : ITimer
    {

        public event Action OnCompleted;

        private float _targetTime;
        private CancellationTokenSource _cancellationTokenSource;


        public void StartTimer(float targetTime)
        {
            StopTimer();
            _targetTime = targetTime;
            _cancellationTokenSource = new CancellationTokenSource();
            var token = _cancellationTokenSource.Token;
            UpdateTimer(token).Forget();
        }

        public void StopTimer()
        {
            if (_cancellationTokenSource != null)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = null;
            }
        }

        private async UniTask UpdateTimer(CancellationToken token)
        {
            var timer = TimeSpan.FromSeconds(_targetTime);
            await UniTask.Delay(timer).AttachExternalCancellation(token);
            OnCompleted?.Invoke();
            StopTimer();
        }
    }
}
