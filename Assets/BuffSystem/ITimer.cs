﻿using System;
namespace BuffSystem
{
    public interface ITimer
    {
        event Action OnCompleted;

        void StartTimer(float targetTime);
        void StopTimer();
    }
}