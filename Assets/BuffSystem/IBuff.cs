﻿namespace BuffSystem
{
    public interface IBuff
    {
        string ID { get; }
        CharacterStats ApplyBuff(CharacterStats characterStats);
    }
}
