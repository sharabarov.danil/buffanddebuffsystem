﻿namespace BuffSystem.Example
{

    public class ImmortalityBuff : IBuff
    {
        private string _id;

        public ImmortalityBuff()
        {
            _id = this.ToString().GenerateUUID();
        }
        public string ID => _id;

        public CharacterStats ApplyBuff(CharacterStats baseStats)
        {
            var newStats = baseStats;
            newStats.IsImmortal = true;

            return newStats;
        }
    }
}
