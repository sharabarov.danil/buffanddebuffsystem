﻿
namespace BuffSystem
{
    public struct CharacterStats
    {
        public int Health;
        public int Armor;
        public int Damage;
        public bool IsImmortal;
    }
}
