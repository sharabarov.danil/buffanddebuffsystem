﻿using UnityEngine;
namespace BuffSystem.Example
{
    public class CharacterView : MonoBehaviour
    {

        private Character _myCharacter;

        private void Start()
        {
            var stats = new CharacterStats
            {
                Health = 10,
                Armor = 5,
                Damage = 2,
                IsImmortal = false
            };
            Init(new Character(stats));

            Debug.Log($"Character initialized. Damage: {_myCharacter.CurrentStats.Damage}, Is Immortal: {_myCharacter.CurrentStats.IsImmortal}");
        }

        public void Init(Character character)
        {
            _myCharacter = character;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _myCharacter.AddBuff(new ImmortalityBuff());

                Debug.Log($"Immortallity Enabled: {_myCharacter.CurrentStats.IsImmortal}");
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                _myCharacter.AddBuff(new DamageBuff(2));

                Debug.Log($"Damage: {_myCharacter.CurrentStats.Damage}");
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                var immortalityBuff = new ImmortalityBuff();
                var timer = new Timer();
                var tempImmortalityBuff = new TemporaryBuff(_myCharacter, immortalityBuff, timer, 3);
                _myCharacter.AddBuff(tempImmortalityBuff);

                Debug.Log($"Immortallity Enabled: {_myCharacter.CurrentStats.IsImmortal}");
            }
        }
    }
}

