﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace BuffSystem
{
    public class Character : IBuffable
    {

        public CharacterStats BaseStats { get; }
        public CharacterStats CurrentStats { get; private set; }

        private readonly List<IBuff> _buffs = new();

        public Character(CharacterStats baseStats)
        {
            BaseStats = baseStats;
            CurrentStats = baseStats;
        }

        public void AddBuff(IBuff buff)
        {
            _buffs.Add(buff);

            ApplyBuffs();

            Debug.Log($"Buff added: {buff}");
        }

        public void RemoveBuff(IBuff buff)
        {
            var currentBuff = _buffs.FirstOrDefault(x => x.ID == buff.ID);
            if (_buffs.Contains(currentBuff))
            {
                _buffs.Remove(currentBuff);
                ApplyBuffs();

                Debug.Log($"Buff removed: {buff}");
            }
        }

        private void ApplyBuffs()
        {
            CurrentStats = BaseStats;

            foreach (var buff in _buffs)
            {
                CurrentStats = buff.ApplyBuff(CurrentStats);
            }
        }
    }
}
