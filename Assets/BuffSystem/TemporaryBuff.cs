﻿using UnityEngine;

namespace BuffSystem.Example
{

    public class TemporaryBuff : IBuff
    {
        private readonly IBuffable _owner;
        private readonly IBuff _coreBuff;
        private readonly float _lifeTime;
        private readonly ITimer _timer;
        private readonly string _id;

        public TemporaryBuff(IBuffable owner, IBuff buff, ITimer timer, float lifeTime)
        {
            _owner = owner;
            _coreBuff = buff;
            _lifeTime = lifeTime;
            _timer = timer;
            _id = buff.ID;
        }

        public string ID => _id;

        public CharacterStats ApplyBuff(CharacterStats baseStats)
        {
            var newStats = _coreBuff.ApplyBuff(baseStats);
            _timer.StartTimer(_lifeTime);
            _timer.OnCompleted += () =>
            {
                _owner.RemoveBuff(_coreBuff);
            };
            return newStats;
        }
    }
}
